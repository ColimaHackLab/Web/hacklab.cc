Title: Llavero USB de Software Libre con Gnuk y Más
Date: 2020-05-15 20:21
Tags: GnuPG, Gnuk
Authors: LEXO, hk
Category: Tutoriales
Summary: Llavero USB de Software Libre con Gnuk y Más
Status: published

# Primeros Pasos
________________

### PIN por defecto
PIN = '123456'
Admin PIN = '12345678'

### Paquetes
~~~
sudo apt install gnupg2 scdaemon
~~~

### Restaurar a valores de fábrica
~~~
gpg --card-edit
admin
factory-reset
~~~

### Generar llaves
~~~
gpg --card-edit
admin
generate
list
~~~

### PIN/Contraseña

Por defecto el firmware de Gnuk utiliza un PIN para usuario (123456) y otro para administrador (12345678), tal cual como el estándar, sin embargo es posible utilizar el mismo para ambos (una implementación propia y no tan recomendable, así como que habra que considerar que una vez que se bloquea el PIN de usuario también el admin).

Antes de comenzar es importante tener en cuenta que a la hora de utilizar cualquier PIN, solo tendremos 3 oportunidades, después el llavero quedará bloqueado y solo nos quedará la opción de restaurar a valores de fabrica o utilizar el código de desbloqueo (si es que lo definimos, de ser así es muy importante resguardarlo tanto o más que el PIN de admin y usuaro).

Para definir nuestro/s propio/s PIN/s es necesario que generar llaves o copiar al llavero las existentes (hacer respaldo antes).

PIN individual usuario y administrador:

_Para manejarnos bajo el estándar es necesario primero cambiar el PIN de administrador y luego el de usuario._

~~~
gpg --card-edit
gpg/card> admin 
Admin commands are allowed
gpg/card> passwd 
gpg: OpenPGP card no. D000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 3
Your selection? 1
~~~

PIN único para usuario y administrador

_Para utilizar una única contraseña que sea útil en acciones de usuario y/o administrador solo será necesario (después de generar/mover las llaves) cambiar el PIN de usuario:_

~~~
gpg --card-edit
gpg/card> passwd
~~~

# SSH 
_____

### ~/.gnupg/gpg-agent.conf 
~~~
default-cache-ttl 600
max-cache-ttl 7200
enable-ssh-support
~~~

### Reiniciar gpg-agent
~~~
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
ssh-add -L
~~~


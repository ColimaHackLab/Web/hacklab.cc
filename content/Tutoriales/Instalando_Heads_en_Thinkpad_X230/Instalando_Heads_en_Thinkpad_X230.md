Title: Instalando Heads en Thinkpad X230
Date: 2020-04-10 20:21
Tags: BIOS, Heads, Coreboot
Author: LEXO
Category: Tutoriales
Summary: Guia de instalación de Heads para una Thinkpad X230
Status: published

### Hardware Necesario

1. Programador SPI
2. Clip para SOIC8/SOP8
3. Thinkpad X230

### Paquetes Necesarios 

(Debian Buster)

~~~
flashrom git build-essential zlib1g-dev uuid-dev libdigest-sha-perl libelf-dev bc bzip2 bison flex gnupg iasl m4 nasm patch python wget gnat cpio ccache cmake cmake-data libarchive13 libcurl4 liblzo2-2 libusb-1.0-0 libusb-1.0-0-dev pkg-config texinfo dmidecode bsdiff pv
~~~

### Respaldar BIOS (memoria flash de 4MB y 8MB)

(Se recomienda hacer más de una lectura de cada memoria y corroborar que coincidan haciendo uso de diff, sha256sum, md5sum u otras)

~~~
flashrom -p ch341a_spi -c MX25L6406E/MX25L6408E -r MX25L6406E.rom
flashrom -p ch341a_spi -c MX25L3206E/MX25L3208E -r MX25L3206E.rom
~~~

### Compilar Heads para X230 
~~~
git clone https://github.com/osresearch/heads
cd heads
make BOARD=x230
make BOARD=x230-flash
~~~

### Desbloquear BIOS y Neutralización de ME
~~~
./build/coreboot-4.8.1/util/ifdtool/ifdtool -u MX25L6406E.rom
./build/coreboot-4.8.1/util/me_cleaner/me_cleaner.py -r -t -d -S -O MX25L6406E_cleaned.rom MX25L6406E.rom.new --extract-me extracted_me.rom
~~~

### Gravar ROM Temporal (de flasheo) en la Flash de 4MB MX25L3206E/MX25L3208E (build/x230-flash/x230-flash.rom)
~~~
flashrom -p ch341a_spi -c MX25L3206E/MX25L3208E -w build/x230-flash/x230-flash.rom
~~~

### Iniciar con la ROM Temporal y Flashear Heads (build/x230/coreboot.rom)
~~~
mount -o ro /dev/sda1 /media
flash.sh /media/Heads/x230/coreboot.rom
~~~
